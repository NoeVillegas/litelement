import {LitElement, html} from 'lit-element';

export class MyBanner extends LitElement {
  static get properties(){
    return{
    msj : {type: Object},
    counter: {type: Number}
    }
  }

  constructor(){
    super();
    this.msj = {msj:''}
    this.counter = 0;
  }
  _handlerClick(){
    this.dispatchEvent(new CustomEvent('my-banner-click-banner', {
      detail:{clickCounter: this.counter},
      bubbles: true,
      composed: true}));
  }

  render() {
    return html`
      <div @click="${this._handlerClick}">this is my msj:${this.msj.msj}</div>
    `;
  }
}

customElements.define('my-banner', MyBanner);




