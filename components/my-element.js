import {LitElement, html} from 'lit-element';
import './my-banner.js'

class MyElement extends LitElement {
  static get properties(){
    return{
      propObj :{
        type: Object
      },
      propStr : {type: String},
      propArr : {type: Array},
      propNum : {type: Number}

    }
  }
  constructor(){
   super();
   this.propObj = { msj :'msj parent' };
   this.propStr = "";
   this.propArr = [];
   this.propNum = 0;

  }
  render() {
    return html`
      <div>${this.propStr}</div>
      <label>${this.propNum}</label>
      <ul>
        ${this.propArr.map(elem =>
          html`<li>${elem}</li>`
        )}
      </ul>
      <label>${this.propObj.name}</label>
    `;
  }
  _handleEvent(e){

    console.log(e);
  }

}

customElements.define('my-element', MyElement);




